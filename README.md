Usage:

```
#!bash
monitor.py [-h] [-m] component event_hub_url ip port

positional arguments:
  component      monitoring component: [server, server_backup, auditor, database, database_backup]
  event_hub_url  Event-Hub url
  ip             IP of component
  port           port of component

optional arguments:
  -h, --help     show this help message and exit
  -i, --invoke   performs invoke

python monitor.py server http://event_hub_url 172.16.118.38 1025 -i
```