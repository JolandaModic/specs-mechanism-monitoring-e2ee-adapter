import datetime
import time
import json
import requests
from requests.exceptions import ConnectionError, ConnectTimeout
import logging
import os
import sys
import traceback

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-1.12s][%(levelname)s]  %(message)s")
logger = logging.getLogger()

project_path = os.path.dirname(os.path.realpath(__file__))
fileHandler = logging.FileHandler("{0}/{1}.log".format(os.path.join(project_path), 'monitoring-e2ee-adapter'))
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)
logger.setLevel(logging.INFO)


def send_post(url, data=None, files=None):
    try:
        requests.post(url, data=data, files=files, timeout=10).status_code
    except (ConnectionError, ConnectTimeout) as e:
        logger.error("ERROR SENDING TO: %s" % url)
        logger.error("%s" % traceback.format_exc())


def send_event_report(component, obj, labels, type, value, event_hub_url):
    logger.info("Component: %s, type: %s, value: %s" % (component, type, value))
    timestamp = datetime.datetime.now()
    payload = {
        'component': component,
        'object': obj,
        'labels': labels,
        'token': None,
        'type': "boolean",
        'data': value,
        'timestamp': time.mktime(timestamp.timetuple())
    }
    json_payload = json.dumps(payload)
    send_post(event_hub_url, data=json_payload)