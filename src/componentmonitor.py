import socket

from utils import send_event_report

EVENT = 'event'
REMEDIATION_EVENT = 'event_type_remediation_event'
SERVER = 'specs-mechanism-enforcement-e2ee-server'
AUDITOR = 'specs-mechanism-monitoring-e2ee'
DATABASE = 'postgres'


class ComponentMonitor():
    def __init__(self, event_hub_url, ip, port, component, sla_id, obj, type):
        self.event_hub_url = event_hub_url
        self.ip = ip
        self.port = int(port)
        self.component = component
        self.sla_id = sla_id
        self.obj = obj
        self.type = type

    def monitor_component(self, invoke=False):
        event_type = EVENT
        if invoke:
            event_type = REMEDIATION_EVENT
        s = socket.socket()
        try:
            s.settimeout(3)
            s.connect((self.ip, int(self.port)))
            component_running = True
        except socket.error, e:
            component_running = False
        finally:
            s.close()
        send_event_report(self.component, self.obj, ['sla_id_%s' % self.sla_id, self.type], self.type, component_running, self.event_hub_url)