import argparse
import componentmonitor
from componentmonitor import ComponentMonitor

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("component", help="monitoring component: [server, server_backup, auditor, database, database_backup]")
    parser.add_argument("ip", help="IP of component")
    parser.add_argument("port", help="port of component", type=int)
    parser.add_argument("component_id", help="ID of component")
    parser.add_argument("sla_id", help="SLA_ID")
    parser.add_argument("event_hub_url", help="Event-Hub url")
    parser.add_argument("-i", "--invoke", help="perform invoke", action="store_true")
    args = parser.parse_args()

    if args.component == 'server':
        server = ComponentMonitor(args.event_hub_url, args.ip, args.port, args.component_id, args.sla_id, componentmonitor.SERVER, 'primary_server_availability')
        server.monitor_component(args.invoke)
    if args.component == 'server_backup':
        server_backup = ComponentMonitor(args.event_hub_url, args.ip, args.port, args.component_id, args.sla_id, componentmonitor.SERVER, 'backup_server_availability')
        server_backup.monitor_component(args.invoke)
    if args.component == 'auditor':
        auditor = ComponentMonitor(args.event_hub_url, args.ip, args.port, args.component_id, args.sla_id, componentmonitor.AUDITOR, 'auditor_availability')
        auditor.monitor_component(args.invoke)
    if args.component == 'database':
        database = ComponentMonitor(args.event_hub_url, args.ip, args.port, args.component_id, args.sla_id, componentmonitor.DATABASE, 'primary_db_availability')
        database.monitor_component(args.invoke)
    if args.component == 'database_backup':
        database_backup = ComponentMonitor(args.event_hub_url, args.ip, args.port, args.component_id, args.sla_id, componentmonitor.DATABASE, 'backup_db_availability')
        database_backup.monitor_component(args.invoke)
